#!/bin/bash

base64 -d device/xiaomi/alioth/configs/camera/secret > device/xiaomi/alioth/configs/camera/st_license.lic

# User vars
export BUILD_HOSTNAME=buildserver
export BUILD_USERNAME=gustavomends
export TARGET_KERNEL_BUILD_HOST=ArchLinux
export TARGET_KERNEL_BUILD_USER=GustavoMends

# Remove upload script
rm -rf go-up*
